

 var h1 = document.getElementById("displaytime"),
  start = document.getElementById("start"),
  reset = document.getElementById("reset"),
  pause = document.getElementById("pause"),
  reset_timer =  document.getElementById("displaytime"),
  seconds = 0,
  minutes = 0,
  hours = 0,
  count = 0,
  t = -1;

function timer() {
  t = setTimeout(add, 1000);
}

function add() {  
  seconds++;
  if (seconds >= 60) {
    seconds = 0;
    minutes++;
    if (minutes >= 60) {
      minutes = 0;
      hours++;
    }
  }

  h1.textContent =
    (hours ? (hours > 9 ? hours : "0" + hours) : "00") +
    ":" +
    (minutes ? (minutes > 9 ? minutes : "0" + minutes) : "00") +
    ":" +
    (seconds > 9 ? seconds : "0" + seconds);

  timer();
}

function display_timer() {
  count++;
  console.log(displaytime);
}
setInterval(display_timer ,5000);

start.addEventListener("click", function() {
    seconds = 0; minutes = 0; hours = 0;
    clearInterval(t);
    timer();
});

reset.addEventListener("click", function() {
    clearTimeout(t);
    reset_timer.innerHTML = '00:00:00';
    pause.innerHTML = 'Pause';
});

pause.addEventListener('click', function(e) {
    if (t == -1) {
        pause.innerHTML = 'Pause';
        timer();
    } else {
        pause.innerHTML = "Resume";
        clearInterval(t);
        t = -1;
    }
});


 