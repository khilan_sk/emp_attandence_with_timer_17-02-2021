$(document).ready(function () {
  $("#start").click(function () {
    $("#start").hide();
    $("#pause").show();
    $("#reset").show();
  });
  $("#pause").click(function () {
    $("#reset").show();
  });
  $("#reset").click(function () {
    $("#reset").hide();
    $("#start").show();
    $("#pause").hide();
  });

});

function add() {
  seconds++;
  if (seconds >= 60) {
    seconds = 0;
    minutes++;
    if (minutes >= 60) {
      minutes = 0;
      hours++;
    }
  }

  $('h1').text((hours ? (hours > 9 ? hours : "0" + hours) : "00") +
    ":" +
    (minutes ? (minutes > 9 ? minutes : "0" + minutes) : "00") +
    ":" +
    (seconds > 9 ? seconds : "0" + seconds));
  timer();
}

function timer() {
  t = setTimeout(add, 1000);
}

$(document).on("click", "#start", function () {
  seconds = 0;
  minutes = 0;
  hours = 0;
  t = -1;
  clearInterval(t);
  timer();
});

$(document).on("click", "#reset", function () {
  clearTimeout(t);
  $("#displaytime").text('00:00:00');
  $('#pause').text("Pause");
});

$(document).on("click", "#pause", function () {
  if (t == -1) {
    $('#pause').text("Pause");
    timer();
  } else {
    $('#pause').text("Resume");
    clearInterval(t);
    t = -1;
  }
})


function display_timer() {
  var displaytime = $('#displaytime').text();
  // console.log(displaytime);
}
setInterval(display_timer, 2000);