// first of all we need a boolean to keep track of our timer state 
var active = false;
// now our main function 
function start_timer() {
    // so thisfunction will gof
    active = true;
    if (active) {
        var timer = $("display_time").text();
        var arr = timer.split(":"); // spistting our timer into array by "1", so hour goes to arr [e], minutes goes to arr [1] and so on 
        var hour = arr[0]; //getting hour
        var min = arr[1]; // minutes;  
        var sec = arr[2]; // seconds

        if (sec == 59) {
            if (min == 59) {
                hour++;
                min = 0;
                if (hour < 10) hour = "0" + hour;
            } else {
                min++;
            }
            if (min < 10) min = "0" + min;
            sec = 0;
        } else {
            sec++;
            if (sec < 10) sec = "0" + sec++;
        }
        // update our intel 
        // document.getElementById("display_time").innerHTML = hour + ":" + min + ":" + sec;
        // $("#display_time").val(hour + ":" + min + ":" + sec);
        $("#display_time").text(hour + ":" + min + ":" + sec);
        setTimeout(start_timer, 1000); // keep repeating with speed of 1 second 
    }
}
// }
// now we need function to change states - start or pause timer by clicking 
$(document).on("click", "#start", function changeState() {
    if (active == false) {
        active = true;
        start_timer();
        console.log("Timer has been started");
        $('#start').text("Pause");
    } else {
        active = false;
        console.log("Timer is on pause");
        $('#start').text("Pause");
    }
})
//and Finally our reset
$(document).on("click", "#reset", function reset() {
    // document.getElementById("display_time").innerHTML = "00" + ":" + "00" + ":" + "00";
    $("display_time").text("00" + ":" + "00" + ":" + "00");
    console.log("Timer has been reset ");
})