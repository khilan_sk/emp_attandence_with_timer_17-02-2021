function add() {
  seconds++;
  if (seconds >= 60) {
    seconds = 0;
    minutes++;
    if (minutes >= 60) {
      minutes = 0;
      hours++;
    }
  }

  $('h1').text((hours ? (hours > 9 ? hours : "0" + hours) : "00") +
    ":" +
    (minutes ? (minutes > 9 ? minutes : "0" + minutes) : "00") +
    ":" +
    (seconds > 9 ? seconds : "0" + seconds));
  timer();
}

// function updatetime() {
//   seconds++;
//   if (seconds >= 60) {
//     seconds = 0;
//     minutes++;
//     if (minutes >= 60) {
//       minutes = 0;
//       hours++;
//     }
//   }


// }

function timer() {
  t = setTimeout(add, 1000);
  // t = setInterval(add, 1000);
}

function resume() {
  t = setInterval(add, 1000);
  clearTimeout(t);
  t = -1;
}


$(document).on("click", "#start", function () {
  var arr = timer.split(":");
  var hour = arr[0]; //getting hour
  var min = arr[1]; // minutes;  
  var sec = arr[2];
  t = -1;
  clearInterval(t);
  timer();
});

$(document).on("click", "#reset", function () {
  clearTimeout(t);
  $("#displaytime").text('00:00:00');
  $('#pause').text("Pause");
});

$(document).on("click", "#pause", function () {
  // timer();
  if (t == -1) {
    $('#pause').text("Pause");
    timer();
  } else {
    $('#pause').text("Resume");
    clearInterval(t);
    t = -1;
  }
})


$(document).on("click", "#resume", function () {
  resume();
  if (t == -1) {
    $('#resume').text("Pause");
    timer();
  } else {
    $('#resume').text("Resume");
    clearInterval(t);
    t = -1;
  }
})

function display_timer() {
  var displaytime = $('#displaytime').text();
  $.ajax({
    url: "responce.php",
    type: "POST",
    data: {
      method: "recored_time",
      displaytime: displaytime
    }
  });
}
setInterval(display_timer, 2000);