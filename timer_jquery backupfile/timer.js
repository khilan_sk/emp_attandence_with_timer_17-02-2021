// var h1 = document.getElementById("displaytime"),
// start = document.getElementById("start"),
// reset = document.getElementById("reset"),
// pause = document.getElementById("pause"),
// reset_timer = document.getElementById("displaytime"),
// seconds = 0,
//   minutes = 0,
//   hours = 0,
//   t = -1;

function add() {
  seconds++;
  if (seconds >= 60) {
    seconds = 0;
    minutes++;
    if (minutes >= 60) {
      minutes = 0;
      hours++;
    }
  }

  // h1.textContent =
  $('h1').text((hours ? (hours > 9 ? hours : "0" + hours) : "00") +
    ":" +
    (minutes ? (minutes > 9 ? minutes : "0" + minutes) : "00") +
    ":" +
    (seconds > 9 ? seconds : "0" + seconds));
  timer();
}

function timer() {
  t = setTimeout(add, 1000);
}

/* Start button */
$(document).on("click", "#start", function () {
  // start.addEventListener("click", function () {
  seconds = 0;
  minutes = 0;
  hours = 0;
  t = -1;
  clearInterval(t);
  timer();
});

$(document).on("click", "#reset", function () {
  // reset.addEventListener("click", function () {
  clearTimeout(t);
  // reset_timer.innerHTML = '00:00:00';
  $("#displaytime").text('00:00:00');
  $('#pause').text("Pause");
  // pause.innerHTML = 'Pause';
});

$(document).on("click", "#pause", function () {
  // pause.addEventListener('click', function (e) {
  if (t == -1) {
    // pause.innerHTML = 'Pause';
    $('#pause').text("Pause");
    timer();
  } else {
    // pause.innerHTML = "Resume";
    $('#pause').text("Resume");
    clearInterval(t);
    t = -1;
  }
})


function display_timer() {
  var displaytime = $('#displaytime').text();
  $.ajax({
    url: "responce.php",
    type: "POST",
    data: {
      method: "recored_time",
      displaytime: displaytime
    }
  });
}
setInterval(display_timer, 2000);