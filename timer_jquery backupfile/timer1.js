var active = false;

function start_timer() {
    if (active) {
        var timer = document.getElementById("display_time").innerHTML;
        var hour = arr[0]; //getting hour
        var min = arr[1]; // minutes;  
        var sec = arr[2]; // seconds

        if (sec == 59) {
            if (min == 59) {
                hour++;
                min = 0;
                if (hour < 10) hour = "0" + hour;
            } else {
                min++;
            }
            if (min < 10) min = "0" + min;
            sec = 0;
        } else {
            sec++;
            if (sec < 10) sec = "0" + sec++;
        }

        $("#display_time").text(hour + ":" + min + ":" + sec);
        setTimeout(start_timer, 1000);
    }
}

$(document).on("click", "#start", function changeState() {
    if (active == false) {
        active = true;
        start_timer();
        console.log("Timer has been started");
        $('#start').text("Pause");
    } else {
        active = false;
        console.log("Timer is on pause");
        $('#start').text("Start");
    }
})
//and Finally our reset
$(document).on("click", "#reset", function reset() {
    document.getElementById("display_time").innerHTML = "00" + ":" + "00" + ":" + "00";
    // $("display_time").text("00" + ":" + "00" + ":" + "00");
    console.log("Timer has been reset ");
})